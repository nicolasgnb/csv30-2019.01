import PIL
import Image
import sys
import math

if len(sys.argv) < 3:
    sys.exit('usage: gs.py <input> <output>')

img = cv2.imread('./img/2.bmp')
width, height, _ = img.shape

for y in range(0, height):
    for x in range(0, width):
        p = input_img.getpixel((x, y))
        d = math.sqrt(math.pow(p[0], 2) + math.pow((p[1] - 255), 2) + math.pow(p[2], 2))

        if d > 128:
            d = 255

        output_img.putpixel((x, y), (p[0], min(p[2], p[1]), p[2], int(d)))