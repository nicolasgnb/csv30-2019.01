import numpy as np
import cv2

img = cv2.imread('./img/2.bmp')
background = cv2.imread('./img/background.jpg')

height, width, _ = img.shape
background = cv2.resize(background,(width,height))
# background_color = np.uint8([[[0, 255, 0]]])
# hls_background_color = cv2.cvtColor(background_color, cv2.COLOR_BGR2HLS)
# hls_background_color = hls_background_color[0][0]

hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
lower_green = np.array([40,50,50])
upper_green = np.array([70,255,255])

mask = cv2.inRange(hsv, lower_green, upper_green)

# hls_image = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)

# hue = hls_image[:, :, 0]

# binary_hue = cv2.inRange(hue, 40, 70)

# mask = np.zeros(hls_image.shape, dtype=np.uint8)

# mask[:, :, 0] = binary_hue
# mask[:, :, 1] = binary_hue
# mask[:, :, 2] = binary_hue

blured = cv2.GaussianBlur(mask, (11, 11), 0)
blured_inverted = cv2.bitwise_not(blured)

# bg_key = cv2.bitwise_and(background, blured)
# fg_key = cv2.bitwise_and(img, blured_inverted)

cv2.imshow('img', blured_inverted)
cv2.waitKey()