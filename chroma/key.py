import numpy as np
import cv2
from math import exp
import os

def normaliza(array, minimo, maximo):
    return (array - minimo)/(maximo - minimo)

def gaussianFit(array):
    aux = 0
    gaussian = np.zeros(256)
    for i in range(len(array)):
        aux += hist[i] * i
    mean = aux / np.sum(array)

    aux = 0
    for i in range(len(array)):
        aux += array[i]*(i - mean)**2
    std = np.sqrt(aux / sum(array))
    std *= 1.7

    for i in range(0, 256):
        gaussian[i] = exp(-(((i-mean)**2) /
                            (2*(std**2))))

    return gaussian                            


files = sorted([f for f in os.listdir('./img/')])
for file in files:
    filename = file.split('.bmp')[0]
    img = cv2.imread('./img/' + file)
    # mascara
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    # print hsv
    lower_green = np.array([35, 80, 80])
    upper_green = np.array([70, 255, 255])
    mask = cv2.inRange(hsv, lower_green, upper_green)

    hist = cv2.calcHist([img], [1], mask, [256], [0, 256])
    norm = normaliza(hist, np.min(hist), np.max(hist))

    # transformar o hist em uma gaussian
    gaussian = gaussianFit(hist)
    gaussian = gaussian*2
    gaussian = np.clip(gaussian, 0, 1)    

    mask2 = mask.copy()
    height, width = mask.shape
    for y in range(height):
        for x in range(width):
            if(mask[y, x] == 0):
                mask2[y, x] = 0
            else:
                mask2[y, x] = gaussian[img[y, x, 1]] * 255         

    fundo = cv2.imread('./background.jpg')
    y, x, _ = img.shape
    fundo = cv2.resize(fundo, (x, y))

    content = np.zeros(img.shape)
    bg = np.zeros(img.shape)

    for y in range(height):
        for x in range(width):
            if(mask2[y, x] == 0):
                content[y, x, :] = img[y, x, :]
            else:
                content[y, x, :] = img[y, x, :] * (1-gaussian[img[y, x, 1]])

            green = content[y, x, 1]
            red = content[y, x, 2]
            blue = content[y, x, 0]
            # TIRANDO ESVERDEADOS
            if green * 2 >= blue + red:
                content[y, x, 1] = (red + blue) / 2

    for y in range(height):
        for x in range(width):
            if(mask2[y, x] == 0):
                pass
            else:
                bg[y, x, :] = fundo[y, x, :] * gaussian[img[y, x, 1]]

    cv2.imwrite('result/' + filename + '_0.bmp', mask)
    cv2.imwrite('result/' + filename + '_1.bmp', mask2)
    cv2.imwrite('result/' + filename + '_2.bmp', content)
    cv2.imwrite('result/' + filename + '_3.bmp', bg)
    cv2.imwrite('result/' + filename + '_final.bmp', content + bg)
    cv2.imwrite('result/' + filename + '_original.bmp', img)
    
    # break
    # mask2 = mask.copy()
    # height, width = mask.shape
    # for y in range(height):
    #     for x in range(width):
    #         blue = img[y, x, 0]
    #         green = img[y, x, 1]
    #         red = img[y, x, 2]
    #         hue = hsv[y, x, 0]
    #         saturation = hsv[y, x, 1]
    #         bright = hsv[y, x, 2]
    #         if(hue >= 35 and hue <= 70 and saturation > 40 and bright > 40):
    #             if(red * blue != 0 and (green * green) / (red * blue) > 1.5):
    #                 mask2[y, x] = 125 
    # cv2.imwrite('result/mask2_' + file, mask2)
