import numpy as np
import cv2
from matplotlib import pyplot as plt
from scipy.stats import norm as normal
from math import *

# parametros
img = cv2.imread('./img/5.BMP')


def normaliza(array, minimo, maximo):
    return (array - minimo)/(maximo - minimo)


# mascara
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
lower_green = np.array([35, 80, 80])
upper_green = np.array([70, 255, 255])

mask = cv2.inRange(hsv, lower_green, upper_green)
cv2.imwrite('mask.jpg', mask)

# hist de frequencias
hist = cv2.calcHist([img], [1], mask, [256], [0, 256])
norm = normaliza(hist, np.min(hist), np.max(hist))

plt.plot(norm)

f1 = open('./hist.txt', 'w+')
for i in range(len(hist)):
    f1.write(str(int(hist[i][0])))
    f1.write("\n")
f1.close()

# transformar o hist em uma gaussian
aux = 0
gaussian = np.zeros(256)
for i in range(len(hist)):
    aux += hist[i] * i
mean = aux / np.sum(hist)

#index_max = np.argmax(hist)
#mean = (index_max+mean)/2

aux = 0
for i in range(len(hist)):
    aux += hist[i]*(i - mean)**2
std = np.sqrt(aux / sum(hist))
std *= 1.7

for i in range(0, 256):
    gaussian[i] = exp(-(((i-mean)**2) /
                        (2*(std**2))))
print(mean)
print(std)
plt.plot(gaussian)

# fazer aquele crop massa
gaussian = gaussian*2
gaussian = np.clip(gaussian, 0, 1)

plt.plot(gaussian)
plt.show()

# criar mask2
mask2 = mask.copy()
height, width = mask.shape
for y in range(height):
    for x in range(width):
        if(mask[y, x] == 0):
            mask2[y, x] = 0
        else:
            mask2[y, x] = gaussian[img[y, x, 1]] * 255
'''
cv2.imshow('mask2.jpg', mask2)
cv2.waitKey()
'''
cv2.imwrite('mask2.jpg', mask2)

# juntar as img
fundo = cv2.imread('./img/background.jpg')
y, x, _ = img.shape
fundo = cv2.resize(fundo, (x, y))

out1 = np.zeros(img.shape)
out2 = np.zeros(img.shape)
out3 = np.zeros(img.shape)
height, width, _ = img.shape

for y in range(height):
    for x in range(width):
        if(mask2[y, x] == 0):
            out1[y, x, :] = img[y, x, :]
        else:
            out1[y, x, :] = img[y, x, :] * (1-gaussian[img[y, x, 1]])

        # TIRANDO ESVERDEADOS
        # if(out1[y, x, 1] >= 1*out1[y, x, 2] and out1[y, x, 1] >= 1*out1[y, x, 0]):
        if(2*out1[y, x, 1] >= (1*(out1[y, x, 2]+out1[y, x, 0]))):
            out1[y, x, 1] = (out1[y, x, 2]+out1[y, x, 0])/2

for y in range(height):
    for x in range(width):
        if(mask2[y, x] == 0):
            pass
        else:
            out2[y, x, :] = fundo[y, x, :] * gaussian[img[y, x, 1]]
#[0,0,255]#
out3 = out1 + out2

cv2.imwrite("out1.jpg", out1)
cv2.imwrite("out2.jpg", out2)
cv2.imwrite("out3.jpg", out3)
