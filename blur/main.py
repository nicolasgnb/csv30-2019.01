'''
    Authors: Marcelo Guimarães e Nícolas Messias
'''

import numpy as np
import cv2 as cv

def verifica_pixel(y, x, height, width):
    return (x >= 0) and (x < width) and (y >= 0) and (y < height)


def blur_ingenuo(img_source, janela):
    output = img_source.copy()
    metade_janela = int(janela / 2)
    height, width = img_source.shape
    for y in range(0, height):
        for x in range(0, width):
            soma = 0
            cont = 0
            for y2 in range(y - metade_janela, y + metade_janela +1):
                for x2 in range(x - metade_janela, x + metade_janela+1):
                    if(verifica_pixel(y2, x2, height, width)):
                        soma += img_source[y2, x2]
                        cont += 1
            output[y, x] = soma / cont
    return output


def blur_separavel(img_source, janela):
    output_y = img_source.copy()
    metade_janela = int(janela / 2)
    height, width = img_source.shape
    for y in range(0, height):
        for x in range(0, width):
            soma = 0
            cont = 0
            for y2 in range(y - metade_janela, y + metade_janela+1):
                if(verifica_pixel(y2, x, height, width)):
                    soma += img_source[y2, x]
                    cont += 1
            output_y[y, x] = soma / cont

    output = output_y.copy()
    for y in range(0, height):
        for x in range(0, width):
            soma = 0
            cont = 0
            for x2 in range(x - metade_janela, x + metade_janela+1):
                if(verifica_pixel(y, x2, height, width)):
                    soma += output_y[y, x2]
                    cont += 1
            output[y, x] = soma / cont
    return output


def blur_integral(img_source, janela):
    height, width = img_source.shape
    integral = np.zeros(shape=(height,width), dtype='int64')
    j2 = int(janela / 2)

    for y in range(0, height):
        for x in range(0, width):
            integral[y, x] = img_source[y, x]
            if verifica_pixel(y - 1, x, height, width):
                integral[y, x] += integral[y - 1, x]
            if verifica_pixel(y, x - 1, height, width):
                integral[y, x] += integral[y, x - 1]
            if verifica_pixel(y - 1, x - 1, height, width):
                integral[y, x] -= integral[y - 1, x - 1]

    output = img_source.copy()

    for y in range(0, height):
        for x in range(0, width):

            temp_y, temp_x = 0,0
            if(y+j2>=height):
                temp_y = height-1
            else:
                temp_y = y+j2

            if(x+j2>=width):
                temp_x = width-1
            else:
                temp_x = x+j2

            a = integral[temp_y, temp_x]            
            b = integral[y+j2, x-j2-1] if verifica_pixel( y+j2,x-j2-1, height, width) else 0
            c = integral[y-j2-1, x+j2] if verifica_pixel( y-j2-1,x+j2, height, width) else 0
            d = integral[y-j2-1, x-j2-1] if verifica_pixel( y-j2-1,x-j2-1, height, width) else 0
            soma = a - b - c + d

            temp1, temp2 = x+j2,x-j2

            if(temp1 >= width):
                temp1 = width-1

            if(temp2<0):
                temp2 = 0

            width_janela = temp1 + 1 - temp2
            temp1, temp2 = y, height - y

            if(y-j2>=0):
                temp1 = j2

            if(y+j2<height):
                temp2=j2

            height_janela = temp1 + 1 + temp2

            
            cont = width_janela * height_janela
            output[y,x] = soma/cont

    return output#[janela: height - janela, 0: width - janela]


img = cv.imread('large.jpg', 0)

#out_ingenuo = blur_ingenuo(img, 1)
#cv.imwrite('out_ingenuo.jpg', out_ingenuo)

#out_separavel = blur_separavel(img, 11)
#cv.imwrite('out_separavel.jpg', out_separavel)

out_integral = blur_integral(img, 11)
cv.imwrite('out_integral.jpg', out_integral)