import numpy as np
import cv2
import os

FILE_PATH = '/home/nmessias/Documents/dataset/baseline/office/input/';

# cap = cv2.VideoCapture(0)
# ret, frame = cap.read()


# bg = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
# bg = cv2.GaussianBlur(bg, (9, 9), 0)
i = 0
files = sorted([f for f in os.listdir(FILE_PATH)])
bg = cv2.imread(FILE_PATH + files[0])
# fgbg = cv2.bgsegm.createBackgroundSubtractorMOG()
bg = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
bg = cv2.GaussianBlur(bg, (9, 9), 0)
for file in files:
    img = cv2.imread(FILE_PATH + file)

    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (9, 9), 0)

    difference = cv2.absdiff(gray, bg)

    _, mask = cv2.threshold(difference, 25, 255, cv2.THRESH_BINARY)
    # fgmask = fgbg.apply(img)
    cv2.imshow("mask", mask)
    k = cv2.waitKey(30) & 0xFF

    # if the `q` key is pressed, break from the lop
    if k == 27:
        break

# bg = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
# bg = cv2.GaussianBlur(bg, (9, 9), 0)

# while (1):
#     ret, frame = cap.read()

#     gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#     gray = cv2.GaussianBlur(gray, (9, 9), 0)

#     difference = cv2.absdiff(gray, bg)

#     _, mask = cv2.threshold(difference, 25, 255, cv2.THRESH_BINARY)

#     cv2.imshow("mask", mask)
#     k = cv2.waitKey(30) & 0xFF

#     # if the `q` key is pressed, break from the lop
#     if k == 27:
#         break

# cap.release()
cv2.destroyAllWindows()