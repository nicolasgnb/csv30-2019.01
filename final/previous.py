import numpy as np
import cv2

cap = cv2.VideoCapture(0)
ret, frame = cap.read()

bg = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
bg = cv2.GaussianBlur(bg, (9, 9), 0)

while (1):
    ret, frame = cap.read()

    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (9, 9), 0)

    difference = cv2.absdiff(gray, bg)

    _, mask = cv2.threshold(difference, 4, 255, cv2.THRESH_BINARY)

    cv2.imshow("mask", mask)
    k = cv2.waitKey(30) & 0xFF

    if k == 27:
        break
    bg = gray

cap.release()
cv2.destroyAllWindows()