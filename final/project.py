import numpy as np
import cv2

class BackgroundCalc:
	def __init__(self,alpha,firstFrame):
		self.alpha  = alpha
		self.backGroundModel = firstFrame

	def getForeground(self,frame):
		self.backGroundModel =  frame * self.alpha + self.backGroundModel * (1 - self.alpha)
		return cv2.absdiff(self.backGroundModel.astype(np.uint8),frame)

cam = cv2.VideoCapture(0)

def blur(frame):
    frame = cv2.medianBlur(frame,5)
    frame = cv2.GaussianBlur(frame,(5,5),0)

    return frame

ret,frame = cam.read()
background = BackgroundCalc(0.05, blur(frame))

while(True):
	ret,frame = cam.read()

	if ret is True:
		foreGround = background.getForeground(blur(frame))

		ret, mask = cv2.threshold(foreGround, 15, 255, cv2.THRESH_BINARY)
		cv2.imshow('mask',mask)
		key = cv2.waitKey(10) & 0xFF
	else:
		break

	if key == 27:
		break

cam.release()
cv2.destroyAllWindows()