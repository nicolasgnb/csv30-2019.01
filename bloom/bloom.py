import numpy as np
import cv2 as cv


def im2double(im):
    im = np.float32(im)
    im = im/255
    return im


def im2int(im):
    im = im*255
    im = np.int16(im)
    return im


def truncFloatImg(im):
    if(np.any(im < 0)or np.any(im > 1)):
        width, height, channel = im.shape
        for y in range(0, height):
            for x in range(0, width):
                for c in range(0, channel):
                    if(im[x, y, c] < 0):
                        im[x, y, c] = 0
                    if(im[x, y, c] > 1):
                        im[x, y, c] = 1
    return im


def mascara(source, threshold):
    mask = source.copy()
    width, height, channel = source.shape
    for y in range(0, height):
        for x in range(0, width):
            normal = mask[x, y, 0] * 0.299 + \
                mask[x, y, 1] * 0.587 + mask[x, y, 2] * 0.114
            # normal = mask[x, y, 0] * 3 + mask[x, y, 1] * 3 + mask[x, y, 2] * 3
            # normal/=9

            if(normal <= threshold):  # or (1-normal)<0.09):
                for c in range(0, channel):
                    mask[x, y, c] = 0
                    # mask[x, y, c] = source[x, y, c] if source[x, y, c] > threshold else 0
    return mask


def sumImgs(imgs, weights):
    out = np.zeros(imgs[0].shape, dtype=np.float64)

    for i in range(0, len(imgs)):
        out += imgs[i]*weights[i]

    return out


def bloomMedia0(img, mask, pesos):
    # mascaras com filtro da media (cada mascara utiliza a ultima como base)
    blur_masks = []
    last_mask = mask.copy()
    for i in range(0, 4):
        for _ in range(0, 3):
            last_mask = cv.blur(last_mask, (19, 19))
        blur_masks.append(last_mask.copy())
        cv.imwrite('mask0B'+str(i)+'.jpg', im2int(truncFloatImg(blur_masks[i])))

    b0_mask = sumImgs(blur_masks, [1, 1, 1, 1])
    return sumImgs([img, b0_mask], pesos)


def bloomMedia1(img, mask, pesos, fatorJanela):
    # mascaras com filtro da media (cada mascara utiliza a inicial como base)
    blur_masks = []
    for i in range(0, 4):
        last_mask = mask.copy()
        for _ in range(0, 3):
            last_mask = cv.blur(
                last_mask, (int((19*(i+1))*fatorJanela), int((19*(i+1))*fatorJanela)))
        blur_masks.append(last_mask.copy())
        cv.imwrite('mask1B'+str(i)+'.jpg', im2int(truncFloatImg(blur_masks[i])))

    b1_mask = sumImgs(blur_masks, [1, 1, 1, 1])
    return sumImgs([img, b1_mask], pesos)


def bloomGaussiana(img, mask, pesos, fatorSigma):
    # mascaras com filtro da gaussiana
    blur_masks = []
    last_mask = mask.copy()
    sigma = 10
    for i in range(0, 4):
        last_mask = cv.GaussianBlur(last_mask,sigmaX=(1+2*sigma),sigmaY=(1+2*sigma),ksize=(0,0))
        blur_masks.append(last_mask.copy())
        sigma *= fatorSigma
        cv.imwrite('maskG'+str(i)+'.jpg', im2int(truncFloatImg(blur_masks[i])))

    b0_mask = sumImgs(blur_masks, [1, 1, 1, 1])
    return sumImgs([img, b0_mask], pesos)


# pegar imagem
img = im2double(cv.imread('mtk.jpg'))

# testando funcoes de soma e conversao
temp = truncFloatImg(im2double(cv.imread('mtk.jpg')))
cv.imwrite('test_conversions.jpg', im2int(
    sumImgs([temp, temp], [0.5, 0.5])))

# gerar mascara
mask = mascara(img, 0.7)
cv.imwrite('mask.jpg', im2int(mask))

out0B = bloomMedia0(img, mask, [0.8, 0.4])
cv.imwrite('out0B.jpg', im2int(truncFloatImg(out0B)))

out1B = bloomMedia1(img, mask, [0.8, 0.4], 1)
cv.imwrite('out1B.jpg', im2int(truncFloatImg(out1B)))

outG = bloomGaussiana(img, mask, [0.8, 0.4], 2)
cv.imwrite('outG.jpg', im2int(truncFloatImg(outG)))
# mascaras com fitro gaussiano
