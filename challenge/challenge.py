import numpy as np
import cv2 as cv

img = cv.imread('60.bmp', 0)
img2 = cv.medianBlur(img, 7)
kernel = np.ones((3, 3),np.uint8)
img2 = cv.morphologyEx(img2, cv.MORPH_GRADIENT, kernel)
cv.imshow('oi', img2)
cv.waitKey()
_, img2 = cv.threshold(img2, 25, 255, cv.THRESH_BINARY)

arrozes = []
contours, _ = cv.findContours(img2, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
print len(contours)
for contour in contours:
        areaArroz = cv.contourArea(contour)
        if areaArroz > 9:
                arrozes.append(areaArroz)

TAM = 35
MAX = int(max(arrozes)) + 1

faixas = []

for i in range(0, MAX, TAM):
        faixas.append([(i, i + TAM), []])

for arroz in arrozes:
        for faixa in faixas:
                if arroz > faixa[0][0] and arroz <= faixa[0][1]:
                        faixa[1] .append(arroz)

maior = faixas[0]
for faixa in faixas:
        if(len(maior[1]) < len(faixa[1])):
                maior = faixa
moda = np.median(maior[1])               
cont = 0
for arroz in arrozes:
        if arroz >= (moda * 1.4):
                cont += arroz/moda
        else:
                cont += 1        

print int(cont) + 1
